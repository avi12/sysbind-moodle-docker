# Moodle testing Docker Env
## Preperations
### MoodleNet Frontend
1. get moodlenet front repo
     ```shell script
    git clone git@gitlab.com:moodlenet/frontend.git /your/workspacedir/frontend 
    ```
1. Build docker image
    ```shell script
    cd /your/workspacedir/frontend
    docker build -t {replace with your docker registry}/moodlenet-front:latest .
    ```
### Sysbind Moodle Docker
1. create all Environment variable
    1. copy .env.example to .env
        ```shell script
        cp .env.example .env
        ```
1. Add you ssl cert to  ./dockerfiles/nginx/ssl and esay way is to use [mkcert](https://github.com/FiloSottile/mkcert)
